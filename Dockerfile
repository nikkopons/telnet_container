FROM ubuntu
LABEL MAINTAINER="Nicolás Pons" \
      EMAIL="nikkopons@gmail.com"

EXPOSE 80
EXPOSE 443
RUN apt-get update && apt-get install telnet -y && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["telnet"]  

#docker build -t telnet_container .
#docker run --rm -it -p 80:80 -p 443:443 telnet_container
#docker run --rm -it telnet_container www.ua.es 80
#docker run --rm -it telnet_container www.ua.es 443